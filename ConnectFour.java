import java.util.Scanner;

public class ConnectFour {

    char[][] b;

    public ConnectFour() {
        b = new char[6][7];

        for(int i=0; i<b.length; i++) {
            for(int j=0; j<b[i].length; j++) {
                b[i][j] = ' ';
            }
        }
    }

    boolean isOver() {
        for(int i=0; i<b[0].length; i++) {
            if(b[0][i] == ' ') {
                // this place is vacant
                return false;
            }
        }
        return true;
    }

    public void printBoard() {
        for(int i=0; i<b.length; i++) {
            System.out.printf("|");
            for(int j=0; j<b[i].length; j++) {
                System.out.printf("%c|", b[i][j]);
            }
            System.out.println();
        }
        for(int j=0; j<b[0].length; j++) {
            System.out.printf("--");
        }
        System.out.println("-");
    }

  
    public boolean checkWin() {
        
        boolean foundRow = false;
        boolean foundCol = false;
        boolean foundMjrD = false;
        boolean foundMinD = false;

      
        for (int r = 0; r <= 5; r++) {
            for (int c = 0; c <= 3; c++) {
                if (b[r][c] == b[r][c + 1] && b[r][c] == b[r][c + 2] && b[r][c] == b[r][c + 3] && b[r][c] != ' ') {
                    foundRow = true;
                    break;
                }
            }
        }

        // Check to see if four columns in the same row match
        // check columns
        for (int r = 0; r <= 2; r++) {
            for (int c = 0; c <= 6; c++) {
                if (b[r][c] == b[r + 1][c] && b[r][c] == b[r + 2][c] && b[r][c] == b[r + 3][c] && b[r][c] != ' ') {
                    foundCol = true;
                    break;
                }
            }
        }

        
        for (int r = 0; r <= 2; r++) {
            for (int c = 0; c <= 3; c++) {
                if (b[r][c] == b[r + 1][c + 1] && b[r][c] == b[r + 2][c + 2] && b[r][c] == b[r + 3][c + 3]
                        && b[r][c] != ' ') {
                    foundMjrD = true;
                    break;
                }
            }
        }

        
        for (int r = 0; r <= 2; r++) {
            for (int c = 3; c <= 6; c++) {
                if (b[r][c] == b[r + 1][c - 1] && b[r][c] == b[r + 2][c - 2] && b[r][c] == b[r + 3][c - 3]
                        && b[r][c] != ' ') {
                    foundMinD = true;
                    break;
                }
            }
        }

        
        if (foundRow || foundCol || foundMjrD || foundMinD)
            return true;
        else
            return false;
    } 

    public boolean drop(char disk, int column) {
        if(column < 0 || column > 6) {
            throw new ArrayIndexOutOfBoundsException("There is no mentioned column to drop the disk.");
        }
        boolean put = false;
        for(int i=b.length - 1; i >= 0; i--) {
            if(b[i][column] == ' ') {
                b[i][column] = disk;
                put = true;
                break;
            }
        }

        return put;
    }



    public static void main(String[] args) {
        ConnectFour game = new ConnectFour();

       
        boolean turn = true;

        Scanner in = new Scanner(System.in);

        do {
            turn = !turn;
            game.printBoard();
            char disk;

            if(turn) {
                disk = 'Y';
                System.out.print("Drop a yellow disk at column (0-6): ");
            } else {
                disk = 'R';
                System.out.print("Drop a red disk at column (0-6): ");
            }

            boolean status = game.drop(disk, in.nextInt());
            if(!status) {
                System.out.println("Could not drop disk. Invalid position or Position is already filled.");
            }
            System.out.println();
        } while(!game.isOver() && !game.checkWin());

        game.printBoard();
        if(game.checkWin()) {
            System.out.printf("\nThe %s player has won.\n", (turn ? "yellow" : "red"));
        } else {
            System.out.println("Game is over.");
        }

        in.close();
    }

}
